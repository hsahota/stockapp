from database import DataBase
from pymongo import MongoClient

f = open('nasdaq.csv', 'r')


client = MongoClient('localhost', 27017)
db = DataBase(client.userdb, client.sessiondb, client.companydb)

db.removeCompanyList()


ctr = 0
for line in f:
	if (ctr == 0):
		ctr = ctr + 1  # skip the header line
		continue
	splits = line.split('|')
	symbol = splits[0].strip()
	company_name = splits[1].strip()
	print "Adding", symbol, ",", company_name
	db.addCompanyStockSymbol(symbol, company_name)

f.close()

# f = open('nyse.csv', 'r')
# ctr = 0
# for line in f:
# 	if (ctr == 0):
# 		ctr = ctr + 1  # skip the header line
# 		continue
# 	splits = line.split(',')
# 	symbol = splits[0].translate(None, '\"')
# 	company_name = splits[1].translate(None, '\"')
# 	print "Adding", symbol, ", ", company_name
# 	db.addCompanyStockSymbol(symbol, company_name)

# f.close()
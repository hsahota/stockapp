

import pymongo
from pymongo import MongoClient
import threading
import random


class DataBase():
    
    # lock = threading.Lock()

    def __init__(self, userdb, sessiondb, companydb):
        self.userdb = userdb
        self.sessiondb = sessiondb
        self.companydb = companydb
        self.userdbposts = self.userdb.posts
        self.sessiondbposts = self.sessiondb.posts
        self.companydbrecords = self.companydb.records
        global lock
        lock = threading.Lock()
        global client
        client = MongoClient('localhost', 27017)

    def removeCompanyList(self):
        self.companydbrecords.remove()
        
    def removeUsersAndSessions(self):
        self.userdbposts.remove()
        self.sessiondbposts.remove()


    def addCompanyStockSymbol(self, symbol, company):
        if not symbol or not company:
            raise Exception('Privded symbol name or company name is empty')
        # global lock        
        lock.acquire()
        try:
            record = {'symbol': symbol, 'company': company}
            self.companydbrecords.insert(record)
        finally:
            lock.release()

    def getCompaniesMatching(self, text):
        x =  self.companydbrecords.find({"symbol": {'$regex' : '^' +
             text.upper() + '.*'}}).sort("symbol", 1)
        results = []
        for i in range(min(10, x.count())):
            y = x.next()
            results.append({'symbol': y['symbol'], 'company': y['company']})
        #results = [{'symbol': 'TSLA', 'company': 'Tesla Motors, Inc.'}]
        return results

    def getRandomCompanies(self, number):
        results = self.companydbrecords.find({}, {"symbol": True}).limit(-1)
        symbols = []
        count = results.count()
        if (number > count):
            raise Exception("There are only %s companies. You asked for: %s" %(count, number))
        indeces = random.sample(range(1,count), number)
        indeces.sort()
        prev = 0
        for index in indeces:
            # symbols.append(results.skip(index-prev).next())
            symbols.append(self.companydbrecords.find({}, {"symbol": True}).limit(-1).skip(index-prev).next()["symbol"])
            prev = index
        return symbols
        

    def addSession(self, username, session_id):
        # global lock
        post = {'session_id': session_id, 'username': username}
        lock.acquire()
        success = False
        try:            
            success = self.sessiondb.posts.insert(post)
        finally:
            lock.release()
        if not success:
            raise Exception('Unable to add session')


    def deleteSession(self, session_id):
        lock.acquire()
        try:
            self.sessiondbposts.remove({'session_id': session_id})
        finally:
            lock.release()

    def getCashForUser(self, username):        
        cash = float(self.userdbposts.find_one({'username' : username})['cash'])
        return cash

    def addNewUser(self, name, username, email, code, password):
        post = {"name": name, "username": username, "email": email, "password": password, "code": code, "cash": 100000, "verified": "false"}
        print post
        if (self.userdbposts.find_one({"username": username}) != None):
            raise Exception("Username already exists.")
        self.userdbposts.insert(post)

    def getCodeForUser(self, username):
        userdata = self.userdbposts.find_one({'username' : username})
        print userdata
        return userdata["code"]
    def userVerified(self, username):
        userdata = self.userdbposts.find_one({'username' : username})
        userdata["verified"] = "true"
        userdata.pop("code")
        self.userdbposts.remove({"username": username})
        self.userdbposts.insert(userdata)

    def getGainForUser(self, username):        
        userdata = self.userdbposts.find_one({'username' : username})
        if (userdata.has_key('gain')):
            return userdata['gain']
        return 0.0

    def getUserData(self, username):
        return self.userdbposts.find_one({'username': username})

    def getUserNameForSession(self, session_id):        
        sessionpost = self.sessiondbposts.find_one({'session_id': session_id})
        if (sessionpost == None):
            return None
        return self.userdbposts.find_one({'username' : sessionpost['username']})['username']

    def getPositionsForUser(self, username):
        post = self.userdbposts.find_one({'username' : username})
        positions = []
        if (post.has_key('positions') == True):
            positions = post['positions']
        return positions

    def updateUserCashAndShares(self, username, symbol, shares, company, pricepershare, buy):
        lock.acquire()
        try:
            userdata = self.userdbposts.find_one({'username' : username})
            gain = 0.0
            oldCash = self.getCashForUser(username)
            averagePrice = pricepershare
            newShares = shares if (buy) else 0

            if (buy == True):
                newCash = oldCash - shares * pricepershare
                if (newCash < 0):
                    raise ValueError('User does not have enough cash to buy so many shares')
            else:
                newCash = oldCash + shares * pricepershare

            if (userdata.has_key('positions')):            
                positions = userdata['positions']
                position = self.getPosition(username, symbol)
                print "position = ", position
                if (len(position) != 0):
                    print "position = ", position
                    oldPrice = position['averagePrice']
                    oldShares = position['shares']
                    if (buy == True):
                        averagePrice = (oldShares * oldPrice + shares * pricepershare) / (oldShares + shares)
                        newShares = oldShares + shares
                    else:
                        gain = shares * (pricepershare - oldPrice)
                        print "Computed gain = ", gain
                        averagePrice = oldPrice
                        newShares = oldShares - shares
                    print "removing position = ", position
                    positions.remove(position)
                print "symbol = ", symbol, ", shares = ", newShares, ", averagePrice = ", averagePrice
                if (newShares > 0):
                    positions.append({'symbol': symbol, 'shares': newShares, 'averagePrice': averagePrice, 'company': company})
                if (newShares < 0):
                    raise ValueError('Userdata is unexpected state, number of shares is negative')
            else:
                if (buy == True):
                    positions = [{'symbol': symbol, 'shares': shares, 'averagePrice': averagePrice, 'company': company}]
                else:
                    raise ValueError('Trying to sell shares user does not own')   

            userdata['positions'] = positions
            userdata['cash'] = newCash
            if (userdata.has_key('gain')):
                userdata['gain'] = userdata['gain'] + gain
            else:
                userdata['gain'] = gain
            print "updated positions = ", positions
            self.userdbposts.update({'username': username}, userdata)
        finally:
            lock.release()


    def getPosition(self, username, symbol):
        post = self.userdbposts.find_one({'username': username})
        if (post.has_key('positions')):
            positions = post['positions']
            listOfPositionsMatching = list((position for position in positions if position['symbol']==symbol))
            if(len(listOfPositionsMatching) == 1):
                return listOfPositionsMatching[0]
            if (len(listOfPositionsMatching) == 0):
                return {}
            else:
                raise ValueError("More than one entries found for position " + FB)
    def getUserPositionsAndGain(self, username):
        userdata = self.userdbposts.find_one({'username': username})
        positions = []
        gain = 0.
        if (userdata.has_key('positions')):
            positions = userdata['positions']
        cash = userdata['cash']
        if (userdata.has_key('gain')):
            gain = userdata['gain']
        return {'cash': cash, 'gain': gain, 'positions': positions}
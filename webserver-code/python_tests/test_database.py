import sys
sys.path.append("/root/code/stockapp/webserver-code/")

import unittest
from pymongo import MongoClient
from database import DataBase

class DataBaseTest(unittest.TestCase):
    def setUp(self):
        global client
        client = MongoClient('localhost', 27017)
        client.testcompanydb.records.drop()
        client.testuserdb.posts.drop()
        client.testsessiondb.posts.drop()
        global db
        db = DataBase(client.testuserdb, client.testsessiondb, client.testcompanydb)
    
    def test_adds_company_symbol_to_company_records(self):
        companySymbol = 'ANY'
        companyName = 'ANY_COMPANY'
        cursor = client.testcompanydb.records.find()
        assert cursor.count() == 0

        db.addCompanyStockSymbol(companySymbol, companyName)

        cursor = client.testcompanydb.records.find({"symbol": companySymbol })
        assert cursor.count() == 1
        result = cursor.next()
        assert result["symbol"] == "ANY"
        assert result["company"] == "ANY_COMPANY"

    def test_raises_exception_when_symbol_or_company_is_empty(self):
        cursor = client.testcompanydb.records.find()
        assert cursor.count() == 0
        self.assertRaises(Exception, db.addCompanyStockSymbol, "", "ANY_COMPANY")
        self.assertRaises(Exception, db.addCompanyStockSymbol, "ANY_SYMBOL", "")

    def test_gets_companies_matching_prefix(self):
        SYMBOLS = ["PREFIXA", "PREFIXB", "PREFIXC", "NOPREFIX"]
        COMPANIES = ["X", "Y", "Z", "XY"]
        
        results = db.getCompaniesMatching("PREFIX")
        assert len(results) == 0

        for i in range(4):
            db.addCompanyStockSymbol(SYMBOLS[i], COMPANIES[i])

        results = db.getCompaniesMatching("PREFIX")
        assert len(results) == 3
        assert {'company': 'X', 'symbol': "PREFIXA"} in results
        assert {'company': 'Y', 'symbol': "PREFIXB"} in results
        assert {'company': 'Z', 'symbol': "PREFIXC"} in results

    def test_adds_session_id_for_user_and_deletes(self):
        USER = "ANY_USER"
        SESSION_ID = "ANY_SESSION_ID"

        USER1 = "ANOTHER_USER"
        SESSION_ID1 = "ANOTHER_SESSION_ID"

        db.addSession(USER, SESSION_ID)
        db.addSession(USER1, SESSION_ID1)

        cursor = client.testsessiondb.posts.find({})
        assert cursor.count() == 2
        found = False
        for i in range(2):
            result = cursor.next()
            if (result["session_id"] == SESSION_ID and result["username"] == USER):
                found = True
        assert found == True

        db.deleteSession(SESSION_ID)        

        cursor = client.testsessiondb.posts.find({})
        assert cursor.count() == 1
        found = False
        for i in range(1):
            result = cursor.next()
            if (result["session_id"] == SESSION_ID and result["username"] == USER):
                found = True
        assert found == False

    def test_adds_new_user_with_hundred_grand_in_cash(self):
        USERNAME = "username"
        PASSWORD = "password"
        NAME = "name"
        db.addNewUser(NAME, USERNAME, PASSWORD)

        cursor = client.testuserdb.posts.find({"username": USERNAME})
        assert cursor.count() == 1
        result = cursor.next()
        assert result["username"] == USERNAME
        assert result["name"] == NAME
        assert result["cash"] == 100000

    def test_fails_to_add_user_when_already_present(self):
        USERNAME = "username"
        PASSWORD = "password"
        NAME = "name"
        db.addNewUser(NAME, USERNAME, PASSWORD)
        self.assertRaises(Exception, db.addNewUser, NAME, USERNAME, PASSWORD)

    def tearDown(self):
    	print ("tear down")

if __name__ == "__main__":
    unittest.main()
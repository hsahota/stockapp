import urllib2

class StockRetreiver():

	def get_stock_quote(symbolList):
		symbols = "+".join(symbolList)
		pars = ["s", "l1", "d1", "c1", "h", "g", "v", "t8", "j6", "k5", "k", "j"]
		# pars = "sl1d1c1hgvt8"
		lines = urllib2.urlopen("http://download.finance.yahoo.com/d/quotes.csv?s=" + symbols + "&f="+"".join(pars))
		quotes = []
		for line in lines:
			quote = {}
			split = line.translate(None,"\"\n").split(",")
			quote["Symbol"] = split[pars.index("s")]
			quote["LastTrade"] = split[pars.index("l1")]
			quote["LastTradeDate"] = split[pars.index("d1")]
			quote["DayHigh"] = split[pars.index("h")]
			quote["DayLow"] = split[pars.index("g")]
			quote["Volume"] = split[pars.index("v")]
			quote["Target"] = split[pars.index("t8")]
			quote["PercOff52WeekLow"] = split[pars.index("j6")]
			quote["PercOff52WeekHigh"] = split[pars.index("k5")]
			quote["52WeekLow"] = split[pars.index("j")]
			quote["52WeekHigh"] = split[pars.index("k")]
			quotes.append(quote)
		return quotes

get_stock_quote(["AAPL"])
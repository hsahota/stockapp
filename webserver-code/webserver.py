import random
import string
import pymongo
import cherrypy
import json
import imp
from database import DataBase
import simplejson
import urllib2
import smtplib
import sys
import myconfig
import os

from jinja2 import Environment
from jinja2 import FileSystemLoader
from pymongo import MongoClient
from yahoo_finance import Share


class StockAppServer():
    _cp_config = {'tools.staticdir.on' : True,
                  'tools.staticdir.root' : os.path.abspath("..")+"/",
                  'tools.staticdir.dir' : "./"
    }
    env = Environment(loader=FileSystemLoader(os.path.abspath("..") + '/templates'))
    client = MongoClient('localhost', 27017)
    db = DataBase(client.userdb, client.sessiondb, client.companydb)
    username = 'stocksheroteam@gmail.com'
    password = myconfig.password
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.ehlo()
    server.starttls()
    server.login(username,password)

    @cherrypy.expose
    def home(self, username='', password='', msg = ''):

        if cherrypy.session.get('session_string') != None:
            username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
            if (username == None):
                raise cherrypy.HTTPRedirect("/")
            else:
                name = self.db.getUserData(username)['name']
                positions = self.stocksOwned()

                return self.env.get_template('home/home.template.html').render(name=name, positions = positions,
                    cash = self.db.getCashForUser(username = username), msg = msg)

        if (cherrypy.request.headers.has_key('Content-Length')):
            cl = cherrypy.request.headers['Content-Length']
            rawbody = cherrypy.request.body.read(int(cl))
            body = simplejson.loads(rawbody)
            print body
            username = body['username']
            password = body['password']
            print "username: ", username, ", password", password
        else:
            raise cherrypy.HTTPRedirect("/")
        
        userdata = self.db.getUserData(username)
        
        if (userdata == None):
            return json.dumps({"error": "User does not exist!"})
        elif (userdata['password'] == password):
            name = userdata['name']
            session_string = ''.join(random.sample(string.hexdigits, 8))
            cherrypy.session['session_string'] = session_string
            self.db.addSession(username, session_string)
            positions = self.stocksOwned()

            return self.env.get_template('home/home.template.html').render(name=name, positions = positions, cash = self.db.getCashForUser(username = username), msg = msg)
        else:
            return json.dumps({"error": "Wrong Password!"})

    @cherrypy.expose
    def buytransaction(self):
        cl = cherrypy.request.headers['Content-Length']
        rawbody = cherrypy.request.body.read(int(cl))
        body = simplejson.loads(rawbody)
        symbol = body['symbol']
        company = body['company']
        shares = body['shares']
        pricepershare = body['pricepershare']
        total = body['total']

        if cherrypy.session.get('session_string') == None:
            return self.index(msg = "Your session has expired..Please login again.")

        share = Share(symbol)
        username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
        
        self.db.updateUserCashAndShares(username = username, symbol = symbol, company = company, shares = shares, pricepershare= pricepershare, buy = True)
        return self.stocksOwned()
        

    @cherrypy.expose
    def price(self, symbol):
        share = Share(symbol)
        print "getting price of symbol:", symbol
        return json.dumps([symbol, float(share.get_price())])

    @cherrypy.expose
    def selltransaction(self):
        if cherrypy.session.get('session_string') == None:
            return self.index(msg = "Your session has expired..Please login again.")
        cl = cherrypy.request.headers['Content-Length']
        rawbody = cherrypy.request.body.read(int(cl))
        body = simplejson.loads(rawbody)
        symbol = body['symbol']
        company = body['company']
        shares = body['shares']
        pricepershare = body['pricepershare']
        share = Share(symbol)
        username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))

        if (int(shares) <= 0):
            return json.dumps({"error": "Number of shares must be a positive integer."})
        
        self.db.updateUserCashAndShares(username = username, symbol = symbol, company = company, shares = shares, pricepershare = pricepershare, buy = False)
        return self.stocksOwned()

    @cherrypy.expose
    def symbol(self, symbol):
        if cherrypy.session.get('session_string') == None:
            return self.index(msg = "Your session has expired..Please login again.")
    
        name = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
        share = Share(symbol)
        price = share.get_price()
        if (price == None):
            msg = "Please provide a valid security symbol."
            return self.home(msg = msg)
        return self.env.get_template('symbol.html.template').render(symbol=symbol, name = name, price = share.get_price())

    @cherrypy.expose
    def logout(self):
        self.db.deleteSession(cherrypy.session.pop('session_string'))
        raise cherrypy.HTTPRedirect("/")

    @cherrypy.expose
    def index(self, msg=""):
        if cherrypy.session.get('session_string') != None:
            raise cherrypy.HTTPRedirect("/home")

        return self.env.get_template('index.template.html').render(message=msg)

    @cherrypy.expose
    def getStockPriceOfSymbols(self, symbolList=[]):
        responseLines = []
        if len(symbolList) == 0:
            return json.dumps({})
        if (isinstance(symbolList, list)):
            responseLines = urllib2.urlopen('http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1d1c1hgv'%("+".join(symbolList))).readlines()
        else:
            responseLines = urllib2.urlopen('http://download.finance.yahoo.com/d/quotes.csv?s=%s&f=sl1d1c1hgv'%(symbolList)).readlines()
        result = []
        for line in responseLines:
            strippedLine = line.replace('"', '').strip()
            splits = strippedLine.split(",")
            result.append({"symbol": splits[0], "price": splits[1]})
        return json.dumps(result)


    @cherrypy.expose
    def addUser(self):
        cl = cherrypy.request.headers['Content-Length']
        rawbody = cherrypy.request.body.read(int(cl))
        body = simplejson.loads(rawbody)
        name = body['name']
        username = body['username']
        password = body['password']
        email = body['email']

        code = "123"
        try:
            self.server.sendmail('stocksheroteam@gmail.com', email, code)
            post_id = self.db.addNewUser(name, username, email, code, password)
        except Exception as e:
            return json.dumps({"error": e.message})

        return json.dumps({"msg": "Verify email by entering the code you received in email."})

    @cherrypy.expose
    def verify(self):
        cl = cherrypy.request.headers['Content-Length']
        rawbody = cherrypy.request.body.read(int(cl))
        body = simplejson.loads(rawbody)
        username = body['username']
        verificationcode = body['verificationcode']
        print "username=%s, verificationcode=%s"%(username, verificationcode)

        try:
            code = self.db.getCodeForUser(username)
            print code
            if (code == verificationcode):
                self.db.userVerified(username)
                return json.dumps({"msg" : "Email verified."})
        except Exception as e:
            return json.dumps({"error": e.message})


        #post_id = self.db.addNewUser(name, username, email, password)
        # Mark email as verified in the database
        

    @cherrypy.expose
    def getUserCashAndPositions(self):
        if cherrypy.session.get('session_string') != None:
            username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
            userdata = self.db.getUserPositionsAndGain(username = username)
            positions = self.db.getPositionsForUser(username = username)
            userdata['positions'] = positions
            return json.dumps(userdata)
        else:
            return {"error" : "session expired"}

    @cherrypy.expose
    def stocksOwned(self):
        if cherrypy.session.get('session_string') != None:
            username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
            positions = self.db.getPositionsForUser(username = username)
            for position in positions:
                position['currentPrice'] = 0.0
            print "positions owned = " + str(positions)
            return json.dumps(positions)
        else:
            return "session expired"

    @cherrypy.expose
    def cash(self):
        if cherrypy.session.get('session_string') != None:
            username = self.db.getUserNameForSession(cherrypy.session.get('session_string'))
            return json.dumps(self.db.getCashForUser(username = username))
        else:
            return "session expired"

    @cherrypy.expose
    def getStocks(self, searchText):
        symbolsMatchingSearch = self.db.getCompaniesMatching(searchText)
        print symbolsMatchingSearch
        return json.dumps(symbolsMatchingSearch)

    @cherrypy.expose
    def nukeDataBases(self):
        self.db.removeUsersAndSessions()

    @cherrypy.expose
    def updateCompanyDB(self):
        f = open('nasdaq.csv', 'r')
        ctr = 0
        for line in f:
            if (ctr == 0):
                ctr = ctr + 1  # skip the header line
                continue
            splits = line.split(',')
            symbol = splits[0].translate(None, '\"')
            company_name = splits[1].translate(None, '\"')
            print "Adding", symbol, ", ", company_name
            self.db.addCompanyStockSymbol(symbol, company_name)

        f.close()

        f = open('nyse.csv', 'r')
        ctr = 0
        for line in f:
            if (ctr == 0):
                ctr = ctr + 1  # skip the header line
                continue
            splits = line.split(',')
            symbol = splits[0].translate(None, '\"')
            company_name = splits[1].translate(None, '\"')
            print "Adding", symbol, ", ", company_name
            self.db.addCompanyStockSymbol(symbol, company_name)

        f.close()

    @cherrypy.expose
    def getRandomStockData(self):
        # companies = ["AAPL", "YHOO"]

        # stock = Share("YHOO")
        data = []
        companies = self.db.getRandomCompanies(3)
        for company in companies:
            stock = Share(company)
            data.append(stock.get_historical("2013-04-25", "2014-04-29"))
        return json.dumps(data)
        # return json.dumps(stock.get_historical("2013-04-25", "2014-04-29"))

if __name__ == '__main__':
    cherrypy.quickstart(StockAppServer(), '/', "config.txt")

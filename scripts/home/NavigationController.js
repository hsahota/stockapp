myApp.controller('NavigationController',
	['$scope', '$http', 'StocksRetreiver', function($scope, $http) {
	
	$scope.logout = function() {
		$http.defaults.headers.post["Content-Type"] = "application/json";
		$scope.logoutmessage = "Logging out ..."

		$http({url: '/logout',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
		}).
		success(function(data, status, headers, config) {
			window.location = "/";
		}).
		error(function(data, status, headers, config) {
			$scope.logoutmessage = "Logout unsuccessful, please try again."
		});
	}
}]);


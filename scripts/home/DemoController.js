myApp.controller('DemoCtrl', function ( $log,
  StocksRetreiver,
  $scope,
  $http) {
	var self = this;

    // list of `state` value/display objects
    // self.states        = loadAll();
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    self.newState = newState;
    
    $scope.numSharesEntered = function() {
    	$scope.costOfBuying = $scope.pps * $scope.numSharesToBuy
    }

    $scope.showBuySubmitButton = function() {
    	if ($scope.numSharesToBuy > 0) {
    		return true;
    	}
    	return false;
    }

    $scope.showPrice = function() {
		// console.log("show price: pps = "+JSON.stringify($scope.pps));
		if (isNaN($scope.pps) || ($scope.pps == 0)) {
			console.log("show price false");
			return false;
		} else {
			console.log("show price true");
			return true;
		}
	}


    $scope.showError = function() {

    	if(isNaN($scope.pps)) {
    		$scope.errorMessage = "Invalid Symbol."
    		return true
    	}
    	if ($scope.pps * $scope.numSharesToBuy > $scope.cash) {
    		$scope.errorMessage = "You do not have enough cash to buy this many shares."
    		return true
    	}
    	$scope.errorMessage = ""
    	return false
    }

    $scope.onBuySubmit = function() {

    	$http.defaults.headers.post["Content-Type"] = "application/json";

    	var responsePromise = $http({url: '../buytransaction',
    		method: 'POST',
    		headers: {'Content-Type': 'application/json'},
    		data: 
    		{
    			symbol: $scope.symbolToBuy,
    			company: $scope.companyTobuy,
    			shares: $scope.numSharesToBuy,
    			pricepershare: $scope.pps,
    			total:  $scope.costOfBuying
    		}
    	})

    	$scope.pps =  0;


    	responsePromise.then(readPositions,function (response) {
    		console.log("buy completed, but error in response")
    	});
    	updateUserData();
    }

    function newState(state) {
    	alert("Sorry! You'll need to create a Constituion for " + state + " first!");
    }
    // ******************************
    // Internal methods
    // ******************************
    /**
     * Search for states... use $timeout to simulate
     * remote dataservice call.
     */
     function querySearch (query) {

      return StocksRetreiver.getStocks("".concat(query)).then(function(response) {
      	return response.data.map(function (item) {
      		return {display: item.symbol, value: item.symbol};
      	});
    	});
  }
  function searchTextChange(text) {
  	$log.info('Text changed to ' + text);
  }

  function selectedItemChange(item) {
  	$log.info('Item changed to ' + JSON.stringify(item));
  	$scope.symbolToBuy = item.value;
  	$scope.companyTobuy = item.display;
  	$scope.numSharesToBuy = ''
  	var responsePromise = $http.get('../price?symbol='.concat(item.value));
  	responsePromise.then(function (response) {
  		if ($scope.symbolToBuy == response.data[0]) {
  			$scope.pps = parseFloat(response.data[1]);
  			$log.info('Price per share ' + JSON.stringify(	$scope.pps));
  			$scope.showPrice();
  		}
  	},function (response) {
  		$log.info(response);
  	});
  }

    /**
     * Build `states` list of key/value pairs
     */
    //  function loadAll() {
    //   var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware,\
    //   Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana,\
    //   Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana,\
    //   Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina,\
    //   North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina,\
    //   South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia,\
    //   Wisconsin, Wyoming';
    //   return allStates.split(/, +/g).map( function (state) {
    //     return {
    //       value: state.toLowerCase(),
    //       display: state
    //     };
    //   });
    // }
    /**
     * Create filter function for a query string
     */
    //  function createFilterFor(query) {
    //   var lowercaseQuery = angular.lowercase(query);
    //   return function filterFn(state) {
    //     return (state.value.indexOf(lowercaseQuery) === 0);
    //   };
    // }
});
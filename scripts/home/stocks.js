var myApp = angular.module('loginApp', ['ngMaterial']);

myApp.config(function($interpolateProvider){
	$interpolateProvider.startSymbol('//');
	$interpolateProvider.endSymbol('//');
});


myApp.factory('StocksRetreiver', function($http){
	var StocksRetreiver = new Object();

	StocksRetreiver.getStocks = function(i) {

		if(i && i.length >= 1)
		{
			var response =  $http.get('../getStocks?searchText='.concat(i));
			return response;
		}
		else
		{
			var response =  $http.get('../getStocks?searchText='.concat(i));
			return response;
		}
	}
	return StocksRetreiver;
});
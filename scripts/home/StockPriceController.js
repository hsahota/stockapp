myApp.controller('StockPriceController', ['$scope', '$http', 'StocksRetreiver', function($scope, $http, StocksRetreiver) {
	
	$scope.priceper = { share: 0 };
	$scope.costOfBuying = 0
	$scope.gain = 0.0;
	$scope.cash = 0.0;
	$scope.currentPrices = {}

	readCurrentPrices = function(response) {
		$scope.predicate = '-gain';
		for (var index in $scope.positions) {
			var position = $scope.positions[index];
			if (position.symbol == response.data[index].symbol) {
				position.currentPrice = parseFloat(response.data[index].price);

				if (position.currentPrice-position.averagePrice>=0) {
					position.changeColor = false;
				} else if (position.currentPrice-position.averagePrice<0){
					position.changeColor = true;
				}
				position.gain = (position.currentPrice-position.averagePrice) * position.shares;
			}
		}
	}

	readPositions = function (response) {
		$scope.positions = response.data;
	}

	updateUserData = function() {
		var promise = $http.get('../getUserCashAndPositions');

		promise.then(function (response) {
			userdata = response.data
			$scope.positions = userdata['positions']
			$scope.gain = userdata['gain']
			$scope.cash = userdata['cash']
			var symbolList = ""
			for (var index in $scope.positions) {
				// var position = $scope.positions[index];

				if (index > 0) {
					symbolList = symbolList+"&"
				}
				symbolList = symbolList + "symbolList=" + $scope.positions[index].symbol
			}
			var stockPricePromise = $http.get('../getStockPriceOfSymbols?'.concat(symbolList))
			stockPricePromise.then(readCurrentPrices, function(response){
				console.log(response);
			});
		},function (response) {
			console.log(response)
		});		
	}

	var responsePromise = $http.get('../stocksOwned');


	responsePromise.then(readPositions,function (response) {
	});
	updateUserData();

	$scope.priceper.sharesell = ''


	$scope.sellsymbolChanged = function() {

		$scope.maxSharesSellable = 0
		if ($scope.positions && $scope.sellsymbol) {
			for (var i = 0; i < $scope.positions.length; i++) {
				if ($scope.sellsymbol.symbol == $scope.positions[i].symbol) {
					$scope.maxSharesSellable= parseInt($scope.positions[i].shares)
					$scope.sellsymbol.company = $scope.positions[i].company
				}
			}
		}

		if ($scope.sellsymbol) {
			var responsePromise = $http.get('../price?symbol='.concat($scope.sellsymbol.symbol));


			responsePromise.then(function (response) {
				if ($scope.sellsymbol.symbol == response.data[0]) {
					$scope.pricepersharesell = parseFloat(response.data[1])
				}
			},function (response) {
			});
		}
	}


	$scope.numSharesToSellEntered = function() {
		$scope.proceedsFromSelling = $scope.pricepersharesell * $scope.numSharesToSell
	}

	$scope.onSellSubmit = function() {

		$http.defaults.headers.post["Content-Type"] = "application/json";

		var responsePromise = $http({url: '../selltransaction',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			data: 
			{
				symbol: $scope.sellsymbol.symbol,
				company: typeof $scope.sellsymbol.company == 'undefined' ? "":$scope.sellsymbol.company,
				shares: $scope.numSharesToSell,
				pricepershare: $scope.pricepersharesell,
				total:  $scope.proceedsFromSelling
			}
		})

		responsePromise.then(readPositions,function (response) {
		});
		updateUserData();
	}

	$scope.stocks = [];
	var r = StocksRetreiver.getStocks("");
	r.then(function(data){
		$scope.stocks = []
		for (var index in data.data) {
			$scope.stocks.push(data.data[index].symbol + ":" + data.data[index].company)
		}
    	 // = JSON.stringify(data.data);
    	});


	$scope.updateStocks = function(typedthings){

		if(typedthings && typedthings.length >= 1) {
			console.log("Do something like reload data with this: " + typedthings );
		}
		var r = StocksRetreiver.getStocks(typedthings);
		r.then(function(data){
      // $scope.stocks = JSON.stringify(data.data);
      $scope.stocks = []
      for (var index in data.data) {
      	$scope.stocks.push(data.data[index].symbol + ":" + data.data[index].company);
      }
  }, function(response) {
  	console.log("Error: " + response)
  });
	}
}]);


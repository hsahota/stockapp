mymodal.directive('modal', function () {
  return {
    templateUrl: 'templates/verification.template.html',
    restrict: 'E',
    transclude: true,
    replace:true,
    scope:false,
    link: function postLink(scope, element, attrs) {
      scope.title = attrs.title;

      scope.$watch(attrs.visible, function(value){
        if(value == true)
          $(element).modal('show');
        else
          $(element).modal('hide');
      });

      $(element).on('shown.bs.modal', function(){
        scope.$apply(function(){
          scope.$parent[attrs.visible] = true;
        });
      });

      $(element).on('hidden.bs.modal', function(){
        scope.$apply(function(){
          scope.$parent[attrs.visible] = false;
        });
      });
    }
  };
});

mymodal.directive('compareTo', [function() {
  return {
    require: "ngModel",
    scope: {
      otherModelValue: "=compareTo"
    },
    link: function(scope, element, attributes, ngModel) {

      ngModel.$validators.compareTo = function(modelValue) {
        return modelValue === scope.otherModelValue;
      };

      scope.$watch("otherModelValue", function() {
        ngModel.$validate();
      });
    }
  };
}]);

mymodal.controller('myCtrl',['$scope', '$http', function($scope, $http){
  $scope.options = {
    chart: {
      type: 'lineChart',
      height: 450,
      margin : {
        top: 20,
        right: 20,
        bottom: 40,
        left: 55
      },
      x: function(d){ return d.x; },
      y: function(d){ return d.y; },
      useInteractiveGuideline: false,
      dispatch: {
        stateChange: function(e){ 
          console.log("stateChange"); 
        },
        changeState: function(e){ 
          console.log("changeState"); 
        },
        tooltipShow: function(e){ 
          console.log("tooltipShow"); 
        },
        tooltipHide: function(e){ 
          console.log("tooltipHide"); 
        }
      },
      xAxis: {
        axisLabel: 'Date',
        tickFormat: function(d) {
          return d3.time.format('%b %d %Y')(new Date(d));
        }
      },
      yAxis: {
        axisLabel: 'Stock Price ($)',
        tickFormat: function(d){
          return d3.format('.02f')(d);
        },
        axisLabelDistance: 30
      },
      callback: function(chart){
        console.log("!!! lineChart callback !!!");
      }
    },
    title: {
      enable: true,
      text: 'Random stock picks of the day'
    },
    subtitle: {
      enable: true,
      text: 'Random stock pick of the day (subtitle)',
      css: {
        'text-align': 'center',
        'margin': '10px 13px 0px 7px'
      }
    },
    caption: {
      enable: false,
      html: '<b>Figure 1.</b> Lorem ipsum dolor sit amet, at eam blandit sadipscing, <span style="text-decoration: underline;">vim adhuc sanctus disputando ex</span>, cu usu affert alienum urbanitas. <i>Cum in purto erat, mea ne nominavi persecuti reformidans.</i> Docendi blandit abhorreant ea has, minim tantas alterum pro eu. <span style="color: darkred;">Exerci graeci ad vix, elit tacimates ea duo</span>. Id mel eruditi fuisset. Stet vidit patrioque in pro, eum ex veri verterem abhorreant, id unum oportere intellegam nec<sup>[1, <a href="https://github.com/krispo/angular-nvd3" target="_blank">2</a>, 3]</sup>.',
      css: {
        'text-align': 'justify',
        'margin': '10px 13px 0px 7px'
      }
    }
  };

  $scope.data = [];

  var responsePromise = $http({url: '../getRandomStockData',
    method: 'GET',
    headers: {'Content-Type': 'application/json'},
    data: 
    {
          // symbol: $scope.sellsymbol.symbol,
          // company: typeof $scope.sellsymbol.company == 'undefined' ? "":$scope.sellsymbol.company,
          // shares: $scope.numSharesToSell,
          // pricepershare: $scope.pricepersharesell,
          // total:  $scope.proceedsFromSelling
        }
      })

  responsePromise.then( function(response) {
    $scope.data = []
    for (var i in response.data) {
      var price = [];
      for (var j in response.data[i]) {
        price.push({x: Date.parse(response.data[i][j].Date), y: response.data[i][j].High})
      }
       $scope.data.push({values: price, key: response.data[i][0].Symbol, color: '#'+(Math.random()*0xFFFFFF<<0).toString(16)});
    }
    
  }
  ,function (response) {
    console.log(response);
  });

}]);

mymodal.controller('LoginController', function ($scope, $http) {


  /**/
  $scope.showLoginModal = false;
  $scope.toggleLoginModal = function(){
    $scope.showLoginModal = !$scope.showLoginModal;
  };

  $scope.showVerificationModal = false;
  $scope.toggleVerificationModal = function(){
    $scope.showVerificationModal = !$scope.showVerificationModal;
  };
  $scope.showRegistrationModal = false;
  $scope.toggleRegistrationModal = function() {
    $scope.showRegistrationModal = !$scope.showRegistrationModal;
  }

  setInfoMessage = function(msg) {
    $scope.infoMessage = msg;
    $scope.errorMessage = "";
  }

  setErrorMessage = function(msg) {
    $scope.errorMessage = msg;
    $scope.infoMessage = "";
  }

  $scope.verification = {};
  $scope.verification.code = "";
  $scope.verification.username = "";

  $scope.verify = function() {
    setInfoMessage("Verifying ...");
    $http.defaults.headers.post["Content-Type"] = "application/json";

    $http({url: '/verify',
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      data: 
      {
        verificationcode: $scope.verification.code,
        username: $scope.verification.username
      }
    }).
    success(function(data, status, headers, config) {
      if(data.error) {        
        setErrorMessage(data.error);        
        return
      } else {
        setInfoMessage(data.msg);
        $scope.cancelRegistration();
      }
    }).
    error(function(data, status, headers, config) {
      console.log(data);
    });
  }

  $scope.user={};
  $scope.user.username="";
  $scope.user.password="";

  $scope.login = function() {
    setInfoMessage("Loggin in...");
    $http.defaults.headers.post["Content-Type"] = "application/json";

    $http({url: '/home',
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      data: 
      {
        username: $scope.user.username,
        password: $scope.user.password
      }
    }).
    success(function(data, status, headers, config) {
      if(data.error) {
        setErrorMessage(data.error)        
        return
      }
      window.location.href = window.location.href+"home"
    }).
    error(function(data, status, headers, config) {
    });
  }


  $scope.createAccount = function() {

    $http.defaults.headers.post["Content-Type"] = "application/json";

    $http({url: '/addUser',
      method: 'POST',
      headers: {'Content-Type': 'application/json'},
      data: 
      {
        username: $scope.registration.user.username,
        name: $scope.registration.user.name,
        email: $scope.registration.user.email,
        password: $scope.registration.user.password
      }
    }).
    success(function(data, status, headers, config) {

      if (data.error) {
        setErrorMessage(data.error);
        return;
      } else {
        setInfoMessage(data.msg);
      }

      $scope.toggleVerificationModal = function(){
    $scope.showVerificationModal = !$scope.showVerificationModal;
  };
    }).
    error(function(data, status, headers, config) {
      $scope.registrationMessage = data
    });
  }
});